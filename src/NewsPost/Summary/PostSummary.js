import React from "react";
import "./PostSummary.css";

const PostSummary = (props) => {
  const { id, dataList } = props;

  //Lấy Data duy nhất trùng với id và tiến hành lấy ảnh và content trong data đó
  const summaryList = dataList.filter((item) => {
    return item.id === id;
  });
  const {
    summary: { img, content },
  } = summaryList[0];

  //Tiến hành render ra màn hình
  return (
    content !== undefined && (
      <div id="summary">
        <img
          className="item-1 summary-item"
          src={`/img/News/${id}/${img.first}`}
          alt=""
        />
        <p className="item-2 summary-item">{content[0].des}</p>
        <p className="item-3 highlight summary-item">{content[1].des}</p>
        <p className="item-4 highlight summary-item">{content[2].des}</p>
        <p className="item-5 summary-item">{content[3].des}</p>
        <img
          className="item-6 summary-item"
          src={`/img/News/${id}/${img.second}`}
          alt=""
        />
      </div>
    )
  );
};
export default PostSummary;
