import React from "react";
import { Link } from "react-router-dom";
import "./PostOthers.css";

const PostOthers = (props) => {
  const { id, dataList, postShow } = props;

  //Tạo list rỗng sẽ chứa data để render
  const otherPostsList = [];

  //Tiến hành tạo vòng lặp while với điều kiện số bài hiện ra sẽ bằng postShow, đồng thời không được hiện bài viết hiện tại, trường hợp nếu đã chạy hết data mà không hiện đủ số bài viết PostShow thì vòng lặp dừng lại
  let index = 0;
  while (otherPostsList.length < postShow) {
    index !== id &&
      otherPostsList.push({
        ...dataList[index].header,
        id: dataList[index].id,
        author: dataList[index].author,
        viewCount: dataList[index].viewCount,
      });
    if (index === dataList.length - 1) {
      break;
    } else {
      index += 1;
    }
  }

  //Tiến hành render từng phần tử trong list Data đã lọc ở bên trên
  const listElement = otherPostsList.map((element) => {
    return (
      <Link to={`/News/${element.id}`} className="post" key={element.id}>
        {/* Dẫn link đến nguồn của img */}
        <img src={`../img/News/${element.id}/${element.img}`} alt=""></img>
        <div className="title">{element.title}</div>
        <div className="other-info">
          <div className="item-author">{element.author}</div>
          <div className="item-viewcount">
            {/* Thêm icon biểu tượng view bên cạnh số view */}
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
              <path d="M288 32c-80.8 0-145.5 36.8-192.6 80.6C48.6 156 17.3 208 2.5 243.7c-3.3 7.9-3.3 16.7 0 24.6C17.3 304 48.6 356 95.4 399.4C142.5 443.2 207.2 480 288 480s145.5-36.8 192.6-80.6c46.8-43.5 78.1-95.4 93-131.1c3.3-7.9 3.3-16.7 0-24.6c-14.9-35.7-46.2-87.7-93-131.1C433.5 68.8 368.8 32 288 32zM432 256c0 79.5-64.5 144-144 144s-144-64.5-144-144s64.5-144 144-144s144 64.5 144 144zM288 192c0 35.3-28.7 64-64 64c-11.5 0-22.3-3-31.6-8.4c-.2 2.8-.4 5.5-.4 8.4c0 53 43 96 96 96s96-43 96-96s-43-96-96-96c-2.8 0-5.6 .1-8.4 .4c5.3 9.3 8.4 20.1 8.4 31.6z" />
            </svg>
            {/* Format số view phù hợp */}
            {Intl.NumberFormat("en-US", {
              notation: "compact",
              maximumFractionDigits: 2,
            }).format(element.viewCount)}
          </div>
        </div>
      </Link>
    );
  });

  return (
    <div className="other-posts">
      <h1 className="header">Other Featured Post</h1>
      <div className="list">{listElement}</div>
    </div>
  );
};

export default PostOthers;
