import React from "react";
import "./PostTitle.css";

const PostTitle = (props) => {
  const { id, dataList } = props;

  //Tiến hành lấy Data có trùng id với props
  const headerList = dataList.filter((item) => {
    return item.id === id;
  });

  //Destructuring để lấy title, text và img từ Data
  const {
    header: { title, text, img },
  } = headerList[0];

  return (
    <div
      id="post-header"
      style={{
        //Dẫn link tới nguồn ảnh dựa vào id và img
        backgroundImage: `url(../img/News/${id}/${img})`,
      }}
    >
      <div className="header-des">
        <h1 className="header-title">{title} </h1>
        <div className="header-text">{text}</div>
      </div>
    </div>
  );
};

export default PostTitle;
