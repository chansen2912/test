import React from "react";
import "./PostContent.css";
import ContentComponent from "./ContentComponent";

const PostContent = (props) => {
  const { id, dataList } = props;

  // Tạo list Data mới bao gồm id, mainContent, author, loại bỏ các thành phần khác
  const contentList = dataList.map((item) => {
    return { ...item.mainContent, author: item.author, id: item.id };
  }, []);
  const { title, list, author } = contentList[id];

  return (
    <div id="main-content">
      <div
        className="title"
        style={
          //Nếu data không có dữ liệu về phần này thì sẽ không hiển thị
          title.img === undefined
            ? { display: "none" }
            : { backgroundImage: `url(../img/News/${id}/${title.img})` }
        }
      >
        <h1>{title.name}</h1>
      </div>

      <div id="list" className="list">
        {/* Render dựa theo ContentComponent */}
        <ContentComponent list={list} postId={id} />
        <div className="author">{`By ${author}`}</div>
      </div>
    </div>
  );
};

export default PostContent;
