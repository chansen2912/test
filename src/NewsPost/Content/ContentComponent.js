import React from "react";

const ContentComponent = (props) => {
  const { list, postId } = props;

  // Dựa vào Data nhận từ component cha, tiến hành render cho từng dòng data
  const listElement = list.map((item) => (
    <div className="content-item" key={item.id}>
      <div className="item-name">
        {/* Nếu data không có tên, thì sẽ ẩn đi, nếu có sẽ hiện lên cùng với vị trí trong danh sách data */}
        {item.name !== "" && item.name}
      </div>

      {/* Nếu data không có ảnh, thì sẽ ẩn đi*/}
      <div className="item-img">
        {item.img !== "" && (
          <img src={`../img/News/${postId}/${item.img}`} alt="" />
        )}
      </div>

      {/* Tiến hành render dựa theo từng phần tử của array trong object có key là des */}
      <div className="item-des">
        {item.des.map((para) => (
          <p key={`${item.id} ${item.des.indexOf(para)}`}>{para}</p>
        ))}
      </div>
    </div>
  ));
  return <>{listElement}</>;
};

export default ContentComponent;
