import React from "react";
import { toast } from "react-toastify";
import InputComponent from "./InputComponent";

const ForgotForm = (props) => {
  const { form, changeForm, dataUser } = props;
  //Lập function kiểm tra email người dùng nhập đã tồn tại trên data hay chưa
  function checkUser() {
    //Lấy email người dùng đã nhập
    const inputUser = {
      email: document.getElementById("forgot-email").value,
    };
    //Lấy data User trong local Storage, nếu không có thì lấy trong data có sẵn
    const usersList =
      localStorage.getItem("usersList") === null
        ? dataUser
        : JSON.parse(localStorage.getItem("usersList"));
    //Kiểm tra trong data User có email nào khớp với inputUser không
    const accExisted = usersList.find(({ email }) => email === inputUser.email);
    //Thông báo tới người dùng
    return accExisted === undefined
      ? toast.warn("Email has not been registerred yet")
      : toast.success(`Password is ${accExisted.password}`);
  }
  return (
    <div id="forgot-form" className={form === "forgot" ? "form-active" : ""}>
      <div className="form-title">Forgot Password</div>
      <form className="form-input" autoComplete="off">
        <InputComponent name="email" type="email" formBelong="forgot" />
        {/* Nút để quay trở về form Sign in */}
        <div className="back">
          <span id="back-signin" onClick={() => changeForm("signin")}>
            Sign in?
          </span>
        </div>
        {/* Nút Submit */}
        <div className="btn-container">
          <button
            type="button"
            id="btn-forgot"
            className="btn-submit"
            onClick={() => checkUser()}
          >
            Submit
          </button>
        </div>
      </form>
    </div>
  );
};

export default ForgotForm;
