import React from "react";

const Title = (props) => {
  const { changeForm, form } = props;
  return (
    <div className="form-header">
      <div
        id="header-signin"
        className={form !== "register" ? "header-active" : ""}
        onClick={() => changeForm("signin")}
      >
        Sign in
      </div>
      <div
        id="header-register"
        className={form === "register" ? "header-active" : ""}
        onClick={() => changeForm("register")}
      >
        Sign up
      </div>
    </div>
  );
};

export default Title;
