import React from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import InputComponent from "./InputComponent";

const SigninForm = (props) => {
  const { changeForm, form, dataUser } = props;

  //SET UP BAN ĐẦU
  localStorage.getItem("isSigninSuccess") === null &&
    localStorage.setItem("isSigninSuccess", "false");

  localStorage.getItem("crAccount") === null &&
    localStorage.setItem("crAccount", "");

  function checkUser() {
    //Lấy dữ liệu email và mật khẩu người dùng đã nhập
    const userInput = {
        email: document.querySelector("#signin-email").value,
        password: document.querySelector("#signin-password").value,
      },
      // Lấy data users trong localStorage, nếu không có thì lấy trong dataUser truyền từ component Signin Page
      usersList =
        localStorage.getItem("usersList") !== null
          ? JSON.parse(localStorage.getItem("usersList"))
          : dataUser,
      //Sử dụng array.find để tìm xem trong userList có data nào khớp với userInput không
      accExisted = usersList.find(
        ({ email, password }) =>
          email === userInput.email && password === userInput.password
      );
    //Trường hợp không tìm thấy data khớp sẽ trả về thông báo "Account is non-existed" nếu khớp sẽ chuyển về trang Home
    if (accExisted === undefined) {
      return toast.warn("Account is non-existed");
    } else {
      return (
        (window.location.href = "/"),
        localStorage.setItem("isSigninSuccess", "true"),
        localStorage.setItem("crAccount", JSON.stringify(accExisted))
      );
    }
  }

  return (
    <div id="signin-form" className={form === "signin" ? "form-active" : ""}>
      <div className="form-title">Sign in</div>
      <form className="form-input" autoComplete="off">
        <InputComponent name="email" type="email" formBelong="signin" />
        <InputComponent name="password" type="password" formBelong="signin" />
        {/* Nút đến với form Forgot */}
        <div className="back" onClick={() => changeForm("forgot")}>
          <span id="back-forgot">Forgot password?</span>
        </div>
        {/* Nút Submit */}
        <div className="btn-container">
          <button
            id="btn-signin"
            type="button"
            className="btn-submit"
            onClick={() => checkUser()}
          >
            Sign in
          </button>
        </div>
      </form>
    </div>
  );
};

export default SigninForm;
