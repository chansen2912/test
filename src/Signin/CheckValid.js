function checkValid(inputTag, name) {
  console.log(inputTag);
  let result = true,
    maxLength = 8;
  switch (name) {
    case "username":
      result =
        inputTag.value.length < maxLength && inputTag.value.length > 0
          ? false
          : true;
      break;
    case "email":
      result =
        !inputTag.value.includes("@") && inputTag.value.length > 0
          ? false
          : true;
      break;
    case "password":
      result =
        inputTag.value.length < maxLength && inputTag.value.length > 0
          ? false
          : true;
      break;
    case "confirm-password":
      result =
        inputTag.value !== document.querySelector("#register-password").value &&
        inputTag.value.length > 0
          ? false
          : true;
      break;
    default:
      result = true;
      break;
  }
  const status = inputTag.parentElement.lastElementChild;
  return result === false
    ? status.classList.add("status-active")
    : status.classList.remove("status-active");
}

export { checkValid };
