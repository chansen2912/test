import React from "react";
import { toast } from "react-toastify";
import InputComponent from "./InputComponent";

const RegisterForm = (props) => {
  const { form, dataUser } = props;
  function checkUser() {
    //Lấy thông tin người dùng đã nhập bao gồm username, email, password, và confirm-password
    const newUser = {
        username: document.getElementById("register-username").value,
        email: document.getElementById("register-email").value,
        password: document.getElementById("register-password").value,
      },
      pwdConfirm = document.getElementById("register-confirm-password").value;
    // Kiểm tra thông tin nhập vào của người dùng, điều kiện tương tự như hàm checkValid, thêm bước này để tránh việc người dùng ignore cảnh báo mà ấn đăng ký
    if (
      newUser.username.length < 8 ||
      newUser.password.length < 8 ||
      !newUser.email.includes("@") ||
      newUser.password !== pwdConfirm
    ) {
      return toast.warn("Please check your input information");
    } else {
      // Lấy danh sách data từ local Storage, nếu không có thì sẽ lấy trong danh sách gốc sẵn có
      const usersList =
        localStorage.getItem("usersList") === null
          ? dataUser
          : JSON.parse(localStorage.getItem("usersList"));
      //Kiểm tra nếu email đã được đăng ký thì sẽ hiện thông báo, còn không thì sẽ reload lại trang
      const accExisted = usersList.find(({ email }) => email === newUser.email);
      if (accExisted !== undefined) {
        //Hiện thông báo nếu email đã được đăng ký
        return toast.warn("Email has been registerred");
      } else {
        //Tiến hành đẩy lại userList đã bao gồm newUser vào local Storage
        usersList.push({ id: usersList.length + 1, ...newUser });
        localStorage.setItem("usersList", JSON.stringify(usersList));
        //Reload lại page
        return window.location.reload();
      }
    }
  }
  return (
    <div
      id="register-form"
      className={form === "register" ? "form-active" : ""}
    >
      <div className="form-title">Register</div>
      <form className="form-input" autoComplete="off">
        <InputComponent name="username" type="text" formBelong="register" />
        <InputComponent name="email" type="email" formBelong="register" />
        <InputComponent name="password" type="password" formBelong="register" />
        <InputComponent
          name="confirm password"
          type="password"
          formBelong="register"
        />
        {/* Nút Submit */}
        <div className="btn-container">
          <button
            id="btn-register"
            type="button"
            className="btn-submit"
            onClick={() => checkUser()}
          >
            Sign up
          </button>
        </div>
      </form>
    </div>
  );
};

export default RegisterForm;
