import React from "react";
import { checkValid } from "./CheckValid";

const InputComponent = (props) => {
  const { name, type, formBelong } = props,
    // Thiết lập định dạng in hoa chữ cái đầu tiên của name để hiện thị ra màn hình
    nameUpper = name
      .split(" ")
      .map((word) => word[0].toUpperCase() + word.substring(1))
      .join(" "),
    // Thiết lập định dạng chữ thường toàn bộ và nối chữ bằng dầu - để định dạng css nếu cần
    nameLower = name
      .split(" ")
      .map((word) => word[0].toLowerCase() + word.substring(1))
      .join("-"),
    // Thiết lập định dạng cho loại thông tin cần điền vào input là chữ thường
    typeLower = type.toLowerCase();

  //Thiết lập nội dung cảnh báo người dùng trong trường hợp người dùng nhập sai thông tin
  let dangerStatus = "";
  switch (nameLower) {
    case "username":
      dangerStatus = "Username is too short";
      break;
    case "email":
      dangerStatus = "Email format is invalid";
      break;
    case "password":
      dangerStatus = "Password's length is too short";
      break;
    case "confirm-password":
      dangerStatus = "Passwords do not match";
      break;
    default:
      dangerStatus = "";
      break;
  }

  return (
    <div className="group form-floating">
      <input
        className={`form-control ${nameLower}`}
        type={typeLower}
        placeholder=" "
        id={`${formBelong}-${nameLower}`}
        onInput={() =>
          checkValid(
            document.querySelector(`#${formBelong}-${nameLower}`),
            nameLower
          )
        }
        required
      />

      <label htmlFor={`${formBelong}-${nameLower}`}>{nameUpper}</label>
      <div className="status">{dangerStatus}</div>
    </div>
  );
};

export default InputComponent;
