import "./Post.css";
import PostTitle from "../../NewsPost/Title/PostTitle";
import PostSummary from "../../NewsPost/Summary/PostSummary";
import PostContent from "../../NewsPost/Content/PostContent";
import PostOthers from "../../NewsPost/OtherPosts/PostOthers";
import React from "react";
import { useParams } from "react-router-dom";
import { AnimationOnScroll } from "react-animation-on-scroll";

function Post(props) {
  //Cài PostId bằng pathname Url
  const { postId } = useParams();
  const { dataList } = props;

  //Sẽ Scroll on top mỗi lần re-render
  window.scroll(0, 0);

  return (
    <>
      <PostTitle id={postId} dataList={dataList} />

      <div className="post-container">
        {/* Thêm animation */}
        <AnimationOnScroll
          animateIn="animate__pulse"
          animateOut="animate__fadeOut"
          duration={0.7}
        >
          <PostSummary dataList={dataList} id={postId} />
        </AnimationOnScroll>

        {/* Thêm animation */}
        <AnimationOnScroll
          animateIn="animate__fadeInUp"
          animateOut="animate__fadeOut"
          duration={0.7}
        >
          <PostContent dataList={dataList} id={postId} />
        </AnimationOnScroll>

        {/* Thêm animation */}
        <AnimationOnScroll
          animateIn="animate__flipInX"
          animateOut="animate__flipOutX"
          duration={0.7}
        >
          <PostOthers dataList={dataList} id={postId} postShow="4" />
        </AnimationOnScroll>
      </div>
    </>
  );
}

export default Post;
