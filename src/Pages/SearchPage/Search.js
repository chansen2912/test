import React, { useState } from "react";
import "./Search.css";
import PostExample from "../../NewsPage/PostExample";

const Search = (props) => {
  const { dataList } = props;
  //Trong trường hợp người dùng muốn quay trở lại từ bài Post về trang Search thì sẽ giữ nguyên kết quả search do lấy từ Session Storage
  const lastFilter = JSON.parse(sessionStorage.getItem("lastFilter"));
  const [filterList, setFilterList] = useState(
    lastFilter === null ? [] : lastFilter
  );

  const filterData = (event) => {
    event.preventDefault();

    //Lấy data từ thông tin người dùng nhập
    const inputSearch = document
      .querySelector(".search-input")
      .value.toLowerCase();

    //Tiến hành lọc data dựa trên data sẵn có và thông tin từ người dùng với function map để lọc lấy các trường dữ liệu cần thiết và hàm filter để tìm data có title giống với data người dùng nhập
    const resultList = dataList
      .map((item) => {
        return {
          ...item.header,
          id: item.id,
          author: item.author,
          viewCount: item.viewCount,
        };
      })
      .filter(({ title }) => title.toLowerCase().includes(inputSearch));
    //Set State về data vừa lọc bên trên
    return setFilterList(resultList);
  };
  //Dựa vào PostExample Component sẵn có tiến hành render data ra màn hình
  const filterElement = filterList.map((element) => {
    return (
      //Render từng component
      <PostExample key={element.id} item={element} />
    );
  });

  //Tiến hành set lại giá trị lastSearch trong Session Storage
  sessionStorage.setItem("lastFilter", JSON.stringify(filterList));
  console.log(filterList);

  return (
    <div className="search-container">
      <h2 className="search-title">Search</h2>
      <form className="search-box">
        <input className="search-input" placeholder="Put some text here" />
        <button type="submit" className="btn-search" onClick={filterData}>
          <svg
            className="logo-search"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 512 512"
          >
            <path d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM241 119c-9.4-9.4-24.6-9.4-33.9 0s-9.4 24.6 0 33.9l31 31H120c-13.3 0-24 10.7-24 24s10.7 24 24 24H238.1l-31 31c-9.4 9.4-9.4 24.6 0 33.9s24.6 9.4 33.9 0l72-72c9.4-9.4 9.4-24.6 0-33.9l-72-72z" />
          </svg>
        </button>
      </form>
      <div className="search-list">{filterElement}</div>
    </div>
  );
};

export default Search;
