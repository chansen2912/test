import React, { useState } from "react";
import Title from "../../Signin/Title";
import SigninForm from "../../Signin/SigninForm";
import ForgotForm from "../../Signin/ForgotForm";
import RegisterForm from "../../Signin/RegisterForm";
import "./SigninPage.css";
import usersList from "../../Data/UsersList";

const SigninPage = () => {
  const [currentForm, setCurrentForm] = useState("signin");

  //Function thay đổi form hiện ra trên màn hình bao gồm Sign in, Register và Forgot
  function changeForm(form) {
    setCurrentForm(form);
  }

  return (
    <div className="signin-container">
      <div className="form-container">
        <Title changeForm={changeForm} form={currentForm} />
        <div className="form-content">
          <SigninForm
            changeForm={changeForm}
            form={currentForm}
            dataUser={usersList}
          />
          <ForgotForm
            changeForm={changeForm}
            form={currentForm}
            dataUser={usersList}
          />
          <RegisterForm form={currentForm} dataUser={usersList} />
        </div>
      </div>
    </div>
  );
};

export default SigninPage;
