import React from "react";
import Recently from "../../NewsPage/Recently";
import MostRead from "../../NewsPage/MostRead";
import All from "../../NewsPage/All";
import { AnimationOnScroll } from "react-animation-on-scroll";
import "./NewPage.css";

const NewPage = (props) => {
  const { dataList } = props;
  return (
    <>
      <div className="class">
        {/* Thêm animation */}
        <AnimationOnScroll
          initiallyVisible={true}
          animateIn="animate__fadeInDown"
          animateOut="animate__fadeOutUp"
          duration={0.5}
        >
          <div className="recently">
            <h2 className="sub-title">Recently</h2>
            <Recently data={dataList} maxPostsShow="4" />
          </div>
        </AnimationOnScroll>

        {/* Thêm animation */}
        <AnimationOnScroll
          initiallyVisible={true}
          animateIn="animate__fadeInDown"
          animateOut="animate__fadeOutUp"
          duration={0.5}
        >
          <div className="most-read">
            <h2 className="sub-title">Most Read</h2>
            <MostRead data={dataList} maxPostsShow="4" />
          </div>
        </AnimationOnScroll>

        {/* Thêm animation */}
        <AnimationOnScroll
          animateIn="animate__zoomIn"
          animateOut="animate__zoomOut"
          duration={0.5}
        >
          <div className="all">
            <h2 className="sub-title">All News</h2>
            <All data={dataList} />
          </div>
        </AnimationOnScroll>
      </div>
    </>
  );
};

export default NewPage;
