import React from "react";
import PostExample from "./PostExample";
import "./style.css";

const Recently = (props) => {
  const { data, maxPostsShow } = props;

  // Tạo list Data mới bao gồm header, id, author và viewCount, loại bỏ các thành phần khác
  const recentlyList = data.map((item) => {
    return {
      ...item.header,
      id: item.id,
      author: item.author,
      issueDate: new Date(item.issueDate),
      viewCount: item.viewCount,
    };
  });

  // Sắp xếp list Data từ cao xuống thấp dựa theo viewCount, sau đó lấy data bằng biến maxPostsShow.
  recentlyList.sort((a, b) => b.issueDate - a.issueDate);
  recentlyList.splice(maxPostsShow);

  // Render data đã lọc bên trên ra màn hình
  const recentlyElement = recentlyList.map((element) => {
    return (
      //Render từng component
      <PostExample key={element.id} item={element} />
    );
  });

  return <div className="recently-list">{recentlyElement}</div>;
};

export default Recently;
