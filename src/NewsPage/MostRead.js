import React from "react";
import PostExample from "./PostExample";
import "./style.css";

const MostRead = (props) => {
  const { data, maxPostsShow } = props;

  // Tạo list Data mới bao gồm header, id, author và viewCount, loại bỏ các thành phần khác
  const mostReadList = data.map((item) => {
    return {
      ...item.header,
      id: item.id,
      author: item.author,
      viewCount: item.viewCount,
    };
  });

  // Sắp xếp list Data từ cao xuống thấp dựa theo viewCount, sau đó lấy data bằng biến maxPostsShow.
  mostReadList.sort((a, b) => b.viewCount - a.viewCount);
  mostReadList.splice(maxPostsShow);

  // Render data đã lọc bên trên ra màn hình
  const mostReadElement = mostReadList.map((element) => {
    return (
      //Render từng component
      <PostExample key={element.id} item={element} />
    );
  });

  return <div className="most-read-list">{mostReadElement}</div>;
};

export default MostRead;
