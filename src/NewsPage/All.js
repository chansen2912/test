import React from "react";
import PostExample from "./PostExample";

const All = (props) => {
  const { data } = props;
  const allNews = data.map((item) => {
    return {
      ...item.header,
      id: item.id,
      author: item.author,
      viewCount: item.viewCount,
    };
  });

  const allElement = allNews.map((element) => {
    return (
      //Render từng component
      <PostExample key={element.id} item={element} />
    );
  });
  return <div className="all-list">{allElement}</div>;
};

export default All;
