let newsList = [
  {
    id: "0",
    author: "Lorem",
    issueDate: "2022/12/31",
    viewCount: "1231",
    header: {
      title: "Vietnamese Tet is coming",
      text: `Tet Nguyen Dan or Tet is the most important and popular holiday and festival in Vietnam. It is the Vietnamese New Year marking the arrival of spring based on the Lunar calendar, a lunisolar calendar. The name Tet Nguyen Dan is Sino-Vietnamese for Feast of the very First Morning.`,
      img: `header-title.jpg`,
    },
    summary: {
      img: {
        first: `summary-1.jpg`,
        second: `summary-2.jpg`,
      },
      content: [
        {
          id: "0",
          des: `Tet is also an occasion for pilgrims and family reunions. During Tet, Vietnamese visits their relatives and temples, forgetting the troubles of the past year and hoping for a better upcoming year. They consider Tet to be the first day of spring and the festival is often called Hội xuân (spring festival).`,
        },
        {
          id: "1",
          des: `Like other Asian countries, Vietnamese believe that the color of red and yellow will bring good fortune, which may explain why these colors can be seen everywhere in Lunar New Year. People consider what they do on the dawn of Tet will determine their fate for the whole year, hence people always smile and behave as nice as they can in the hope for a better year.`,
        },
        {
          id: "2",
          des: `Vietnamese Lunar New Year is NOT Chinese Lunar New Year. It is absolutely mistaken to say that the Vietnamese are celebrating the Chinese Lunar New Year. Although both countries use the lunar calendar, Vietnamese people have different lunar new year customs, celebrations, and traditions from China.`,
        },
        {
          id: "3",
          des: `In the Vietnamese language, to celebrate Tet also means to eat, which affirmed the important role of Vietnam Lunar New Year’s Food. One Vietnamese traditional proverb denotes that you can be hungry all year except three days of Tet festival because dozens of delicious dishes and desserts will be prepared on Lunar New Year in Vietnam to feast the ancestors as well as every family member.`,
        },
      ],
    },
    mainContent: {
      title: {
        name: `Foods for Vietnamese Lunar New Year`,
        img: `main-content-header.jpg`,
      },
      list: [
        {
          id: "0",
          name: `Vietnamese Banh Chung`,
          img: `food-1.jpg`,
          des: [
            `Banh Chung is the traditional cake in Vietnam that only appears on Tet holiday. Tasty and savory Banh Chung is made from very familiar ingredients to all Vietnamese such as sticky rice, green beans, and pork, which is wrapped in green leaves and boiled overnight.`,
            `According to a legend that has been passed on for thousands of generations, Banh Chung is the symbol of Earth because it combines all unique ingredients of Vietnamese agriculture. Although nowadays Banh Chung can be tasted all seasons, enjoying these cakes at Tet holiday still brings special feelings for Vietnamese people.`,
          ],
        },
        {
          id: "1",
          name: `Vietnamese Jam`,
          img: `food-2.jpg`,
          des: [
            `Jam is the common snack to welcome guests on Tet holiday. It is mainly made from dried fruits like carrots, coconuts, apples, or some kinds of seeds such as roasted watermelon seeds, sunflower seeds mixed with sugar.`,
            `The Vietnamese believe eating sweet things on Tet holiday will bring them luck for the Lunar New Year.`,
          ],
        },
        {
          id: "2",
          name: `Vietnamese Pickled Onions`,
          img: `food-3.jpg`,
          des: [
            `On Tet holiday, people often eat fat and oiled foods which easily cloys their appetite. This is high time for something with low calories and fresh like pickled onions. Pickled onions help the digestive system digest high protein food more easily.`,
            `In the minds of Vietnamese people, Tet is only complete when there are fat meat, pickled onions, and red distiches. Besides, there is a myriad of signature dishes that inspire the taste in different regions on Vietnamese Lunar New Year such as spring rolls, boiled chickens, Vietnamese sausages, sticky rice, and so on.`,
          ],
        },
      ],
    },
  },
  {
    id: "1",
    author: "Ipsum",
    issueDate: "2022/12/01",
    viewCount: "2311",
    header: {
      title: "There's the banh mi and there are these variations",
      text: `Vietnam's banh mi has achieved well deserved renown as a delicious street food staple, but Saigon offers the quintessentially Vietnamese sandwich with fillings far more appetizing than the standard one. Even the most jaded palette will be tickled by the six different types of banh mi described below.`,
      img: `header-title.jpg`,
    },
    summary: {},
    mainContent: {
      title: {},
      list: [
        {
          id: "0",
          name: `Banh mi with shumai`,
          img: `food-1.jpg`,
          des: [
            `Shumai balls are made with minced pork, spices and other ingredients to achieve a soft, crunchy, and moist texture. The broth it comes in might be sweet and sour or made with fresh coconut water. You can enjoy the shumai with a few slices of cucumber, coriander and chili pepper after dipping the banh mi in the sauce.`,
            `Recommended spots: 1/17 Hoang Dieu Street (District 4) or the corner of Hoang Dieu – Nguyen Tat Thanh Street; 397 Phan Xich Long (Phu Nhuan District); and 782 Nguyen Kiem (Go Vap District).`,
          ],
        },
        {
          id: "1",
          name: `Banh mi with shredded pork skin`,
          img: `food-2.jpg`,
          des: [
            `Banh mi with shredded pork skin has long been considered Saigon's best breakfast option by sandwich aficionados. The combination of shredded pork skin and sweet and sour fish sauce is unbeatable, foodies say. The delicacy is a mixture of shredded pork skin and roasted rice powder, offering a fragrant, crunchy, flavorful, but not overly rich taste. The seller frequently cuts the banh mi in half and stuffs it with shredded pork skin, pickles, scallion oil and a well-balanced sweet, sour, spicy and salty fish sauce.`,
            `Recommended spots: Corner of Ngo Thoi Nhiem and Nam Ky Khoi Nghia Streets (District 3); 150 Nguyen Trai (District 1).`,
          ],
        },
        {
          id: "2",
          name: `Banh mi with shredded fried fish cakes`,
          img: `food-3.jpg`,
          des: [
            `A loaf of banh mi with shredded fish cakes has become a regular start to a new day for many Saigon residents because it is available across the city. The crunchy, chewy fried fish cake and chili garlic sauce that is stuffed into a hot banh mi is yummy and a perfect start to the day, fans say. To spice things up further, the seller also includes cucumber, Vietnamese coriander, chili sauce, and slices of fresh chili pepper.`,
            `Recommended spots: 12 Nguyen Thi Minh Khai (District 1); 187 Vinh Vien (District 10); 19 Dao Duy Anh (Phu Nhuan District); 68 Le Van Tho (Go Vap District); 35 Phan Boi Chau (Binh Thanh District); and 115 Nguyen Cong Tru (District 1).`,
          ],
        },
        {
          id: "3",
          name: `Banh mi with roasted pork belly`,
          img: `food-4.jpg`,
          des: [
            `Crispy roasted pork belly is the major ingredient of this dish, of course. Its crispy outer part and fatty inner part never fails to win the foodie over. The vendor adds cucumber, pickles, chilli and a special sauce that makes the dish even more flavorful.`,
            `Recommended spots: 3 Nguyen Thuong Hien (District 3); 58 Dong Ho (Tan Binh District); 46 Tran Quy (District 11); 189 Pham Phu Thu (District 6); and 102 Au Co (Tan Binh District).`,
          ],
        },
        {
          id: "4",
          name: `Banh mi with grilled beef wrapped in piper lolot leaves`,
          img: `food-5.jpg`,
          des: [
            `The main component of this banh mi are the tiny, finger-sized beef sausages wrapped in piper lolot leaves to protect the meat from drying out while grilling. Piper lolot is a green heart-shaped leaf that is sometimes confused with the betel leaf; both are members of the pepper family and are used in many Southeast Asian countries.`,
            `This banh mi filling comes with a variety of condiments that rounds the dish off, including cucumber, Vietnamese coriander, pickles, scallion oil and a dash of chili or shacha sauce.`,
            `Recommended spots: opposite Vinh Hoi Apartment (District 4); 364 Nguyen Tat Thanh (District 4); and 1150 Le Duc Tho (Go Vap District).`,
          ],
        },
        {
          id: "5",
          name: `Banh mi with cold cuts`,
          img: `food-6.jpg`,
          des: [
            `Perhaps the most popular banh mi in Saigon is the "ordinary" one with ham and pork bologna sausage. In addition to the crusty outer layer, depending on the store, the seller adds pâté, ham, pork, homemade Vietnamese mayo and shredded chicken, not to mention pickled vegetables, coriander and chili pepper.`,
            `Recommended spots: 363 Hai Ba Trung (District 3); 68 Ham Nghi (District 1); 26 Le Thi Rieng (District 1); and 402/3 Le Van Sy (District 3).`,
          ],
        },
      ],
    },
  },
  {
    id: "2",
    author: "Perspiciatis",
    issueDate: "2022/11/01",
    viewCount: "531",
    header: {
      title: "Balut among world's worst rated egg dishes",
      text: `'Trung vit lon,' or duck eggs with embryos inside, has been listed among the 10 "worst rated egg dishes" in the world by international food magazine TasteAtlas.`,
      img: `header-title.jpg`,
    },
    summary: {},
    mainContent: {
      title: {},
      list: [
        {
          id: "0",
          name: "",
          img: "food-1.jpg",
          des: [
            `'Trung vit lon,' or duck eggs with embryos inside, has been listed among the 10 "worst rated egg dishes" in the world by international food magazine TasteAtlas. The food is popular among several Southeast Asian countries including Vietnam, and is often internationally known by its Filipino name "balut."`,
            `Balut is not only popular in Vietnam but also in some Asian countries such as China, Philippine, and Cambodia. However, Vietnamese balut is left a little longer than that in other countries. It is also cooked, served, and eaten in a different way. Although this kind of food is delicious, cheap, and healthy, it is not popular in other parts of the globe because of the scare of eating the whole unborn duck.`,
            `In Vietnam, duck egg embryos have become a favorite snack that can be found at street-side stalls or food courts. They can be seasoned with chili, garlic, vinegar, salt, lemon juice, ground pepper, and the pungent Vietnamese coriander rau ram.". Recently, the famous culinary website Taste Atlas has put balut on the list of the most “scary” egg dishes on the planet.`,
            `TasteAtlas describes balut as a dish, which is made from eggs that have formed all the parts of a young duck (about 17-21 days old). Eggs are usually boiled, eaten with soup powder and laksa leaves. The reason this dish is popular with locals is that they think it has an invigorating effect. In addition, in the summer, this is also an ideal snack to eat when drinking beer. Balut is also used by restaurants as ingredient to eat hot pot.`,
          ],
        },
        {
          id: "1",
          name: ``,
          img: `food-2.jpg`,
          des: [
            `While boiled balut is a common choice, Vietnamese street food vendors also offer stir-fried egg embryos with tamarind sauce topped with fried onions and peanuts. One fetal duck egg provides 182,000 calories, 13.6 grams of protein, 12.4 grams of lipid, 82 milligrams of calcium, 212 grams of phosphor and 600 milligrams of cholesterol, studies have found. It also provides high amounts of beta carotene, vitamins and iron.`,
            `This dish is said to have originated during the Ming Dynasty in Hunan, when a farmer found duck eggs in a lime tank and decided to try them. Then he added salt to the eggs to improve the taste. Today, the Chinese dish of century eggs is often compared to the rotten cheeses of the West because if you are not used to it, they will have a strong ammonia smell like horse urine. Local people use it as an appetizer or a side dish with rice, meat porridge and pickled ginger.`,
            `Depending on the region, the cook will sometimes add a little spicy chili sauce mixed with lemon juice to add more flavor to the dish. Tainan city in Taiwan (China) is even known as “oyster omelette paradise” because of its location near the sea, so it always makes the best dish from freshly caught oysters.`,
          ],
        },
      ],
    },
  },
  {
    id: "3",
    author: "Dolores",
    issueDate: "2022/12/12",
    viewCount: "2231",
    header: {
      title: "Vietnamese pho breaks into global list of 20 best soups",
      text: `CNN has included pho bo (Vietnamese beef noodle soup) in its list of 20 best soups in the world along with China’s Lanzhou equivalent and Thailand’s Tom yum goong.`,
      img: `header-title.jpg`,
    },
    summary: {
      img: {
        first: `summary-1.jpg`,
        second: `summary-2.jpg`,
      },
      content: [
        {
          id: "0",
          des: `CNN has included pho bo (Vietnamese beef noodle soup) in its list of 20 best soups in the world along with Bouillabaisse of France, Caldo verde of Portugal, Chorba frik of Algeria, Libya and Tunisia, Chupe de camarones of Peru, Gazpacho of Spain, Groundnut soup of West Africa, Gumbo of United States, and Harira of Morocco.`,
        },
        {
          id: "1",
          des: `In the article CNN revealed that the broth is simmered for hours with cinnamon, star anise, and other warm spices in order to create a wonderfully aromatic base for this rice noodle soup.`,
        },
        {
          id: "2",
          des: `CNN said by 1930, the soup was served with slices of raw beef cooked gently in the broth and today pho bo remains the most beloved version in Vietnam, with options that include the original raw beef, a mix of raw and cooked beef, brisket and tendon.`,
        },
        {
          id: "3",
          des: `Pho is made and served across the country though it originated in Hanoi and was taken to the south, where people added their own touches. The noodle soup has been getting rave reviews from global travel magazines for decades. A bowl of pho bo in Ho Chi Minh City and Hanoi cost from VND 30,000 to 50,000 ($1.32-2.20).`,
        },
      ],
    },
    mainContent: {
      title: {},
      list: [],
    },
  },
  {
    id: "4",
    author: "Numquam",
    issueDate: "2022/12/21",
    viewCount: "5221",
    header: {
      title: "Cong Caphe ventures to South Korea",
      text: `Instead of branching out to Germany or Thailand, as had been suggested by business partners, local coffee chain Cong Caphe has established a foothold in Seoul in its first foreign market foray, officially joining the South Korean coffee industry. However, what whether the venture will be successful remains to be seen.`,
      img: `header-title.jpg`,
    },
    summary: {
      img: {
        first: `summary-1.jpg`,
        second: `summary-2.jpg`,
      },
      content: [
        {
          id: "0",
          des: `Hoang Tien, born in the early 90s, has achieved a fair bit of success with his startup Coffee Bike Vietnam. For him, Cong Caphe’s recent foray into the South Korean market has been quite inspiring event for brand promotion.`,
        },
        {
          id: "1",
          des: `“It creates a motivating force for young Vietnamese startups to raise their confidence in building their brands on their home turf. Foreign brands have entered Vietnam to develop their business chains through franchising, and Vietnamese firms can do the same,” Tien said.`,
        },
        {
          id: "2",
          des: `Coffee has grown into an indispensable part of life for many South Koreans. The capital city Seoul features a dense presence of coffee and pastry shop brands, such as Holly’s, Ediya, Angels in the US, Tous Les Jours, Paris Baguette, and Caffe Bene, to name but a few. Those shops either lie right next to each other or are only a few metres removed from one another.`,
        },
        {
          id: "3",
          des: `Vietnamese coffee chain Cong Caphe opened its first South Korean location in Seoul’s Yeonnam-dong Street, a favourite venue for South Korean youths. The main bartender here is an experienced Vietnamese expatriate living in Seoul.`,
        },
      ],
    },
    mainContent: {
      title: {
        name: `Difficulty`,
        img: `main-content-header.jpg`,
      },
      list: [
        {
          id: "0",
          name: `Matching the Korean taste`,
          img: `food-1.jpg`,
          des: [
            `Cong Caphe is now mulling over taking further steps in South Korean market. The coffee chain’s growth story dates back to 2007 with the first shop in Hanoi. It now boasts more than 50 shops across Vietnam.`,
            `To win the heart of customers, the brand has made efforts towards network expansion and brand recognition; the shops feature a retro décor to pique the curiosity of young guests, consistent from table and chair sets, glasses, and cups to the general décor, whereas the beverages will be adapted to fit the host country’s culture.`,
            `Considering the advantages, Cong Caphe’s venture abroad was viewed as ‘promising’ by market observers. Although having a lengthy tradition in tea, South Koreans now drink coffee an average of 12 times a week, even more often than they eat their traditional kim chi dish. This explains why, even though coffee shops have sprung up like mushrooms in the country, there are still enough customers to make operations profitable.`,
            `Korea Customs Service (KCS) figures show that the country’s coffee market value touched $10.8 billion last year, nearly four times the average level of a decade ago. This equals 26.5 billion coffee cups served, averaging 512 cups per capita a year for the population of 51.7 million. Mixed coffees were the most selected beverages, with 13 billion cups served. Running second was freshly roasted coffee, with 4.8 billion cups. The remainder was packed coffee and other coffee-flavoured beverages.`,
          ],
        },
        {
          id: "1",
          name: `High expectations`,
          img: `food-2.jpg`,
          des: [
            `Cong Caphe venturing abroad is good news for the chain, but concerns persist. A decade ago, coffee chain Trung Nguyen, XQ Silk, and Pho 24 also ventured into global markets, but achieved little success. Recently, Wrap and Roll, a subsidiary of Chao Do, started franchising in Australia, Singapore, and China; rice burger VietMac franchised in Germany; and Vietnamese restaurant chain Truly Viet under Redsun began franchising in Australia.`,
            `As fact, the number of Vietnamese brands franchising abroad is very modest compared to the massive entry of foreign food brands into the country’s market. Expanding their footprints abroad is the aspiration of many local firms looking for expansion. However, reaching the global market sustainably requires very careful preparations. Retail and franchise expert Nguyen Phi Van said that to franchise abroad, local firms must dare to make changes and have to share a common vision with and support their partners. In particular, they must build up suitable platforms and follow a clear roadmap for business restructuring and franchise plans in the domestic and foreign market.`,
            `For Cong Caphe, it is too early to say whether its foreign venture will end up successful. As Van, those firms which take control of raw material sources and supply chains will have the upper hand in brand value augmentation when they engage in franchising model at home and abroad.`,
          ],
        },
      ],
    },
  },
  {
    id: "5",
    author: "Nostrum",
    issueDate: "2022/11/01",
    viewCount: "1123",
    header: {
      title: "7 Best Fine-dining Restaurants in Vietnam 2022",
      text: `TripAdvisor - the most reputable travel website - has just announced the top list of the best fine-dining restaurants in Vietnam for this year. All the high-end restaurants listed were in the most popular destinations in Vietnam such as Hanoi, Ho Chi Minh City, Hoi An, and Phan Thiet.`,
      img: `header-title.jpg`,
    },
    summary: {},
    mainContent: {
      title: {},
      list: [
        {
          id: "0",
          name: `Kabin`,
          img: `food-1.jpg`,
          des: [
            `Address: Renaissance Riverside Hotel, No.15 Ton Duc Thang St., Ben Nghe, District 1, Ho Chi Minh City`,
            `Opening hours: 11.30 - 14.30 (lunch) & 18.00 - 22.00 (dinner)`,
            `Price from: VND 250,000 (~USD 8.6)`,
            `Signature dish: Dim Sum & Bun`,
            `The first name mentioned is Kabin, a 5-star restaurant specializing in Cantonese cuisine, located in the Renaissance Riverside Hotel Saigon, Ho Chi Minh City. The restaurant has a prime location as it is located next to the Saigon River and foods are cooked by experienced chefs from Hongkong. Besides many types of delightful dumplings, the dishes that are loved by many dinners here include BBQ Charsiu Pork, Roasted Duck, Steamed Fresh Grouper, and Beef Noodles.`,
          ],
        },
        {
          id: "1",
          name: `Vista Restaurant`,
          img: `food-2.jpg`,
          des: [
            `Address: The Cliff Resort & Residences, No.5 Nguyen Dinh Chieu St., Phu Hai, Phan Thiet, Binh Thuan`,
            `Opening hours: 6.30 - 22.00`,
            `Price from: VND 360,000 (~USD 12.5)`,
            `Signature dish: BBQ Seafood & The Cliff Hotpot`,
            `The second on the list is Vista Restaurant, which is belonged to The Cliff Resort - the most luxurious accommodation in Phan Thiet and the top choice for beach vacations here. The restaurant is spacious with two serving areas: indoor and outdoor and its capacity is around 200-300 diners at the same time. Vista serves seafood, Vietnamese, European and vegan dishes.`,
            `However, its Asian foods, especially local modern-style dishes are always the signatures of this restaurant. Besides serving amazing foods, owning a direct stunning view of the sea is ata Restaurant. `,
          ],
        },
        {
          id: "2",
          name: `Temple Restaurant & Lounge`,
          img: `food-3.jpg`,
          des: [
            `Address: La Siesta Resort & Spa, No.132 Hung Vuong St., Cam Pho, Hoi An, Quang Nam`,
            `Opening hours: 6.30 - 10.00 (breakfast), 11.30 - 22.00 (lunch & dinner), 11.00 - 00.00 (bar/drinks)`,
            `Price from: VND 120,000 (~ USD 4.2)`,
            `Signature dish: Sea Bass, Lamb Chops, and Mango Gazpacho`,
            `Located in one of the most beautiful boutique resorts in Hoi An, the Temple Restaurant design is inspired by the Indochine style and mostly serves European cuisine. The restaurant offers typical dishes from France, and Italy, and also some fusion dishes, especially the ones with ingredients from seafood and tropical fruits.`,
            `Not only food, but drinks are a Temple. While the last order for dining here is at around 21.45, you still can chill here and order drinks until midnight. In addition, this restaurant offers a "Happy Hours" promotion from 16.00 -17.30 & 20.30 – 22.00; therefore, if you want to drink a lot, you can come to the Temple Restaurant & Lounge and chill during this time to save more money.`,
          ],
        },
        {
          id: "3",
          name: `Hemispheres Steak & Seafood Grill`,
          img: `food-4.jpg`,
          des: [
            `Address: Sheraton Hanoi Hotel, K5 Nghi Tam, No.11 Xuan Dieu Rd., Tay Ho, Hanoi`,
            `Opening hours: 18.30 - 22.30 (dinner only)`,
            `Price from: VND 145,000 (~ USD 5)`,
            `Signature dish: Lobster & Ceviche`,
            `Next is Hemispheres Steak & Seafood Grill - a seafood and steak restaurant located in the Sheraton Hotel Hanoi, nearby the most beautiful and biggest lake in Hanoi - West Lake. Although focus mostly on seafood and steak, you can find the international flavor in this restaurant with dishes inspired by both Western and Eastern cuisine.`,
            `The highlight here belongs to dishes from lobsters catch locally in Central Vietnam which create the unique fresh taste of the food. Ceviche and dessert also received many compliments from dinners here. The restaurant is a wonderful choice for anyone who wants to avoid crowds and seeks a quiet and relaxed meal.`,
            `Hemisphere's space is not so small but it is cosy and only has the capacity of around 30 diners. As seats are limited and the restaurant only serves dinner, you should book in advance before coming here.`,
          ],
        },
        {
          id: "4",
          name: `The Rhythms Restaurant`,
          img: `food-5.jpg`,
          des: [
            `Address: 7th floor, No.33 - 35 Hang Dau St., Hoan Kiem, Hanoi`,
            `Opening hours: 6.30 - 22.00`,
            `Price from: VND 350,000 (~ USD 12)`,
            `Signature dish: Beef Steak & Fried Spring Rolls`,
            `Rhythm restaurant is located in Hanoi Old Quarter and serves a special menu with fusion local foods. The goal of the restaurant is to help diners feel the sophistication and charm of Vietnamese cuisine and it seems like they made it.`,
            `Most dishes on The Rhythms Restaurant's menu are elegant combinations of Vietnamese and Western ingredients. Fried Spring Rolls and Beef Steak are must-try here; however, the Chef Special Menu is also an impo restaurant. Besides, also there are not so many options, the vegan menu of The Rhythms is also quite impressive and worth trying for anyone who is vegetarian.`,
          ],
        },
        {
          id: "5",
          name: `Cloud Nine Restaurant`,
          img: `food-6.jpg`,
          des: [
            `Address:Lasiesta Premium, 9th floor, No.27 Hang Be St., Hoan Kiem, Hanoi`,
            `Opening hours: 6.30 - 10.00 (breakfast), 11.30 - 16.00 (lunch), and 16.00-22.00 (dinner)`,
            `Price from: 125,000 VND (~ USD 4.3)`,
            `Signature dish: Dishes with prawns`,
            `Cloud Nine Restaurant is the next representative of Hanoi honored by TripAdvisor. This amazing restaurant is located on the 9th floor of La Siesta Premium Hotel in Hanoi Old Quarter as its name: "Cloud Nine". With a capacity of 80 guests, the restaurant atmosphere is peaceful and quiet, suitable for diners who want a slow-paced meal. The restaurant specializes in serving North Vietnam cuisine and its Western fusion versions - the best choice for experiencing fine-dining Vietnamese food.`,
            `Cloud Nine offers both Set Menu and A La Carte Menu for its guests. However, it is recommended that you should choose the Set Menu for a "real" meal and order some dishes from A La Carte Menu to chill with wine or beer here. The prawn is the most praised ingredient here; therefore, you should have at least 2 dishes with prawns here to have a satisfying culinary experience in Cloud Nine Restaurant.`,
          ],
        },
        {
          id: "6",
          name: `French Grill`,
          img: `food-7.jpg`,
          des: [
            `Address: JW Marriott Hotel Hanoi, No.8 Do Duc Duc Rd., Me Tri, Nam Tu Liem, Hanoi`,
            `Opening hours: 18.00 - 22.30 (Monday - Sunday), 12.00 - 14.00 (Theater A La Table - Sunday only)`,
            `Price from: VND 200,000 (~ USD 6.8)`,
            `Signature dish: Beef Steak, Foie Gras, and Seafood`,
            `French Grill is a high-end restaurant belonging to JW Marriott Hotel Hanoi. Although located in a quite inconvenient location which is way far from the city centre, the culinary experience with French Grill is always worth a try. In fact, all restaurants in JW Marriott Hotel are famous and many diners have praised their food, and as a part of this international 5-star standard hotel, French Grill Restaurant is not an exception. The restaurant specializes in serving seafood, grilled and European dishes. If you love meat, Beef Steak and Foie Gras should be the best choices. On the other hand, the oyster and snow crab are always signature options for seafood here.`,
          ],
        },
      ],
    },
  },
  {
    id: "6",
    author: "Suscipit",
    issueDate: "2022/10/01",
    viewCount: "4567",
    header: {
      title: "Egg beer attracts the curiosity of international tourists",
      text: `Egg beer is one of the specialties of Giang coffee shop beside the egg coffee that once resonated. Egg beer is a mixture of whipped egg yolks with beer, and then a little butter is added to make it rich in flavor and taste. Giang coffee shop’s owner devised a special recipe more than 20 years ago while being curious about what the combination of egg and beer would taste like.`,
      img: `header-title.jpg`,
    },
    summary: {
      img: {
        first: `summary-1.jpg`,
        second: `summary-2.jpg`,
      },
      content: [
        {
          id: "0",
          des: `Giang, one of the most famous decade-old cafés in Hanoi, Vietnam’s capital city, added a delightful beverage egg beer to the menu that impresses a number of locals and foreign visitors, after the café has become well-known for its egg coffee.`,
        },
        {
          id: "1",
          des: `Egg beer is a mixture of whipped egg yolks with beer, and then a little butter is added to make it rich in flavor and taste. With the taste similar to dessert but with the added beer, egg beer not only received support from locals but also become the favorite drink of foreign visitors, including those from beer “power” in Europe.`,
        },
        {
          id: "2",
          des: `The beverage is made of cold beer and egg yolk, whipped with sugar to create creamy and custard-like liquid and added a little bit of butter to boost the taste, AFP reported. Nguyen Chi Hoa, owner of Giang café said that idea of the unique recipe came up in 1999, rooted from his curiosity of the combination between the beer and the egg yolk.`,
        },
        {
          id: "3",
          des: `“I made it just for myself and luckily its taste was good, so I decided to add it to the menu,” he told AFP. Since then, egg beer has drawn attention and become a favorite beverage of foreign visitors. “Taste of the egg beer is wonderful, a perfect combination of the greasy taste of egg and the flavor of beer. We usually drink beer but has never tried this kind of beer before”, Lysiane from France said.`,
        },
      ],
    },
    mainContent: {
      title: {},
      list: [],
    },
  },
  {
    id: "7",
    author: "Dolores",
    issueDate: "2022/12/05",
    viewCount: "2298",
    header: {
      title: "Lẩu cù lao is top of the hotpots in the Mekong Delta",
      text: `Visitors to the Mekong Delta region often rave about the rich and tasty hotpots, with lẩu cù lao being a particular favourite.`,
      img: `header-title.jpg`,
    },
    summary: {},
    mainContent: {
      title: {},
      list: [
        {
          id: "0",
          name: ``,
          img: ``,
          des: [
            `Visitors to the Mekong Delta region often rave about the rich and tasty hotpots, with lẩu cù lao being a particular favourite.`,
            `“Lẩu cù lao (island hotpot) is one of my favourite dishes I’ve enjoyed during my tour to Mekong Delta provinces such as Cần Thơ and Cà Mau,” Nguyễn Thu Phương said.`,
            `Phương, who lives in Germany, said she was unable to come home for the last three years because of the COVID-19 pandemic, even though she missed her parents in Hà Nội.`,
          ],
        },
        {
          id: "1",
          name: ``,
          img: `food-1.jpg`,
          des: [
            `After arriving at home in Tây Hồ District, Phương’s mother told her that she booked tickets for a trip to the Mekong Delta to visit family while enjoying the area's natural beauty and special dishes.`,
            `“I was very happy to have this trip so I could enjoy time with my parents, brothers and sisters as well as our nieces and nephews and enjoy tasty dishes in these provinces,” Phương said.`,
            `When the family arrived in Cần Thơ, they tried the first dish: lẩu cù lao.`,
            `Different from hotpots in the north, the dish here includes more ingredients.`,
          ],
        },
        {
          id: "2",
          name: ``,
          img: `food-2.jpg`,
          des: [
            `They include pig’s heart, liver and marrow, grilled Thát lát fish (a kind of fragrant and sweet fish from the region), duck egg, a piece of dried pork skin, dried squid, dried shrimp, pork paste, cabbage, carrot, cassava, white radish, coriander, salt, sugar, pepper, broth, quality Phú Quốc fish sauce and chili.`,
            `Phương said one of her relatives in Cần Thơ, Huỳnh Thị Minh, knows how to cook the dish, and taught Phương the recipe so that she can make it at home in Germany.`,
            `To make the dish, you first clean the bones, parboil them and pour the water out before pouring two litres of water into the pot and continue to cook for two hours.`,
          ],
        },
        {
          id: "3",
          name: ``,
          img: `food-3.jpg`,
          des: [
            `When the broth is finished, dried shrimp, squid and white radish are added, then salt, sugar, and broth mix, said Minh.`,
            `The pig's heart and liver is chopped into thin slices then parboiled. It is mixed with the fish paste and fresh onion, pepper, and fish sauce. The duck egg is stirred then fried, covered with fish paste, rolled it and cut it into thin threads after steaming.`,
            `“We should soak the dried pig skin for an hour, then add ginger before frying it,” Minh said, adding that the next step is to parboil the cabbage and fresh onion, spread pork paste on the cabbage and roll it, using the parboiled onion to tie it before steaming it for 10 minutes. `,
            `Minh said to have an attractive hotpot, she often uses broccoli and makes carrot and cassava into a flower shapes as a garnish.`,
          ],
        },
        {
          id: "4",
          name: ``,
          img: `food-4.jpg`,
          des: [
            `The last step is to put all ingredients into the pot and wait until it boils. The hotpot is enjoyed with vermicelli or noodles.`,
            `Phương said it was the first time she has enjoyed such a popular but tasty and delicious dish. “The pig's heart and liver are soft but still tough while the broth is sweet and fragrant."`,
            `Minh said the dish was enjoyed by big groups of friends and family. “I still remember that the dish played an important role at my wedding party, 40 years ago, when all the guests enjoyed.”`,
            `Nowadays, the hotpot is often eaten with pork skin threads and fresh herbs wrapped up in rice paper and many others.`,
            `“The hotpot now can be enjoyed year round but the cooking method is different because it depends on each locality and family. For example in An Giang and Đồng Tháp provinces, fish is often added, but in Cà Mau, Bạc Liêu and Kiên Giang provinces, the broth uses shrimp and fresh squid,” Minh said, adding that locals are very proud when visitors and travelers, including foreigners, call lẩu cù lao a specialty they would never forget.`,
          ],
        },
      ],
    },
  },
  {
    id: "8",
    author: "Lorem",
    issueDate: "2022/12/31",
    viewCount: "2231",
    header: {
      title: "Vietnam voted Asia's best culinary destination for first time",
      text: `Industry experts and globetrotters have named Vietnam "Asia's best culinary destination" at this year's World Culinary Awards for the first time.`,
      img: `header-title.jpg`,
    },
    summary: {},
    mainContent: {
      title: {},
      list: [
        {
          id: "0",
          name: ``,
          img: ``,
          des: [
            `The Southeast Asian country surpassed rivals including China, Malaysia, Singapore, South Korea and Thailand to claim the title at the third edition of the awards, the organizing board announced at the recent awards ceremony.`,
            `Rina van Staden, director of the annual awards, said that the results were based on a year-long search for the world's top culinary brands, with both culinary experts and the public voting.`,
          ],
        },
        {
          id: "1",
          name: ``,
          img: `food-1.jpg`,
          des: [
            `The World Culinary Awards is the sister event of the World Travel Awards, which was launched in 1993 to acknowledge excellence in the travel and tourism industry. It has been described as the "travel industry's equivalent of the Oscars. Vietnamese cuisine has become better known around the world over the last couple of years with international chefs and prestigious food magazines praising several national dishes.`,
            `British celebrity chef and television personality Gordon Ramsay recently named Laos and Vietnam the world's top food destinations, calling the latter an "extraordinary melting pot of great food."`,
            `On December 1, the world's most influential restaurant ranking guide Michelin Guide announced it has begun evaluating restaurants in Hanoi and Ho Chi Minh City for the first time ever.`,
          ],
        },
      ],
    },
  },
  {
    id: "9",
    author: "Fugiat",
    issueDate: "2022/12/29",
    viewCount: "2786",
    header: {
      title: "From banh mi to pho: Where to get in Singapore",
      text: `"People are put on earth for various purposes; I was put on earth to do this. Eat noodles right here," said the late, great Anthony Bourdain while slurping down a bowl of noodles in Vietnam.`,
      img: `header-title.jpg`,
    },
    summary: {},
    mainContent: {
      title: {},
      list: [
        {
          id: "0",
          name: ``,
          img: ``,
          des: [
            `Vietnam is a Southeast Asian nation with a rich history. As a result, Vietnamese food is deliciously complex and layered while also being fresh and light thanks to the inclusion of many fresh herbs and vegetables. From the French colonial-influenced baguette sandwich banh mi to soul-nourishing noodle soup pho, here are the places to get authentic and yummy Vietnamese food in Singapore.`,
          ],
        },
        {
          id: "1",
          name: `Cô Chung`,
          img: `food-1.jpg`,
          des: [
            `Cô" translates to "Aunty" and so the store, effectively named Aunty Chung, is named after the co-founder of the brand who happens to be a former nurse. Together with her granddaughter, Co Chung has become a destination for authentic Vietnamese food conveniently placed in the basement of Plaza Singapura and along Boat Quay.`,
            `68 Orchard Rd, #B2-20 Plaza Singapura, Singapore 238839 and 5 Lor Telok, Singapore 049018"`,
          ],
        },
        {
          id: "2",
          name: `Long Phung`,
          img: `food-2.jpg`,
          des: [
            `Ask any Vietnamese where to get good Vietnamese food in Singapore and they'll likely like Long Phung on the list. Long Phung has been around for more than 10 years and its extensive menu coupled with affordable price points make its authentic food even more attractive.`,
            `159 Joo Chiat Rd, Singapore 427436`,
          ],
        },
        {
          id: "3",
          name: `123 Zô Vietnamese BBQ Skewers and Hotpot`,
          img: `food-3.jpg`,
          des: [
            `With 123 Zô, Amy Tran and her Singaporean husband Peter Tan wanted to introduce the underrepresented food of central Vietnam where she is from as well as the country's streetside grill and hotpot culture. Compared to Thai mookata, Vietnamese barbecue is less fatty and features various sauces for different types of food.`,
            `747 Geylang Rd, Singapore 389654`,
          ],
        },
        {
          id: "4",
          name: `Chef Minh Vietnamese Pho`,
          img: `food-4.jpg`,
          des: [
            `Armed with six years of experience as head chef of Macau's Wynn Palace's in-room dining kitchen team, Vietnamese chef Ngo Nhu Minh decided to start his own hawker stall peddling authentic Ho Chi Minh-style pho in 2021. Various beef and chicken pho are available alongside fresh spring rolls.`,
            `279 Bukit Batok East Ave 3, Singapore 650279`,
          ],
        },
        {
          id: "5",
          name: `Banh Mi Saigon`,
          img: `food-5.jpg`,
          des: [
            `Banh Mi Saigon is interestingly sold from a Vietnamese provision shop. The shop was opened by Nhi, whose family has been selling the stuffed sandwich for decades in Ho Chi Minh City. She opened the banh mi arm due to bad business due to the pandemic and has been well-loved for its taste and sizeable portion.`,
            `505 Ang Mo Kio Ave 8, #01-2668, Singapore 560505`,
          ],
        },
        {
          id: "6",
          name: `Joo Chiat Caphe`,
          img: `food-6.jpg`,
          des: [
            `For the uninitiated, Joo Chiat Road is home to many Vietnamese eateries. The latest to throw their name into the ring is Joo Chiat Caphe, opened by business partners Francis Sim and Lynn Tay. The former has always wanted to sell banh mi and the two decided to incorporate the otah they sell (Nam San Mackerel Otah) as a filling option here. Wash your sandwich down with the traditional Vietnamese coffee.`,
            `263 Joo Chiat Rd, Singapore 427517`,
          ],
        },
        {
          id: "7",
          name: `Banh Mi Thit`,
          img: `food-7.jpg`,
          des: [
            `Also in Geylang, Banh Mi Thit's pride and glory is the Vietnamese banh mi sandwich, with options ranging from pork to egg to pate. If you would like a dessert option, get the cold Cheng Tng or Custard Pudding known as Banh Flan. PS: They bake their own baguettes daily.`,
            `543 Geylang Rd, Singapore 389498`,
          ],
        },
      ],
    },
  },
];

export default newsList;
