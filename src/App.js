import React from "react";
import "./index.css";
import Header from "./Header/Header";
import Home from "./Pages/Home/Home";
import Post from "./Pages/NewsPost/Post";
import NewPage from "./Pages/NewsPage/NewsPage";
import Search from "./Pages/SearchPage/Search";
import SigninPage from "./Pages/SigninPage/SigninPage";
import newsList from "./Data/NewsList";
import { Route, Routes } from "react-router-dom";
import { ToastContainer } from "react-toastify";

function App() {
  return (
    <>
      <ToastContainer
        autoClose={5000}
        hideProgressBar={false}
        draggable
        pauseOnHover
        theme="dark"
      />
      <Header />
      <div className="container">
        <Routes>
          {/* Trường hợp đường dẫn vào News */}
          <Route path="/News">
            {/* Nếu không có đường dẫn nào thêm thì sẽ vào trang tổng hợp News Page */}
            <Route index element={<NewPage dataList={newsList} />} />

            {/* Nếu có đường dẫn chi tiết vào bài post thì sẽ tiến hành lấy pathname làm postId để truyền xuống các component con */}
            <Route path=":postId" element={<Post dataList={newsList} />} />
          </Route>
          <Route path="Signin" element={<SigninPage />} />
          <Route path="Search" element={<Search dataList={newsList} />} />
          {/* Trường hợp không thuộc các trường hợp bên dưới thì sẽ mặc định về trang Home */}
          <Route path="*" element={<Home />} />
        </Routes>
      </div>
    </>
  );
}

export default App;
